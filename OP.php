<?php
add_action( 'admin_menu', 'WMOP_option_page_add_menu' );
function WMOP_option_page_add_menu() {
	add_menu_page(
		'Token Merchandise', // page <title>Title</title>
		'Token Merchandise', // menu link text
		'manage_options', // capability to access the page
		'WMOP_option-slug', // page URL slug
		'WMOP_function_option_page', // callback function /w content
		'dashicons-lock', // menu icon
		5 // priority
	);
 
}
add_action( 'admin_init',  'WMOP_option_page_register_setting' );
 
function WMOP_option_page_register_setting(){
 
	register_setting(
		'WMOP_option_settings', // settings group name
		'Token_WMOP_option_settings', // option name
		'sanitize_text_field' // sanitization function
	);
	add_settings_section(
		'WMOP_option_settings_section_id', // section ID
		'', // title (if needed)
		'', // callback function (if needed)
		'WMOP_option-slug' // page slug
	);
 
	add_settings_field(
		'Token',
		'Token',
		'WMOP_option_text_field_html', // function which prints the field
		'WMOP_option-slug', // page slug
		'WMOP_option_settings_section_id', // section ID
		array( 
			'label_for' => 'Token_WMOP_option_settings',
		)
    );
}
function WMOP_option_text_field_html(){
 
	$text = get_option( 'Token_WMOP_option_settings' );
 
	printf(
		'<input type="text" id="Token_WMOP_option_settings" name="Token_WMOP_option_settings" value="%s" />',
		esc_attr( $text )
	);
 
}
function WMOP_function_option_page(){
    ?> 
    <div class="wrap">
	    <form method="post" action="options.php">
            <?php
                settings_fields( 'WMOP_option_settings' ); 
                do_settings_sections( 'WMOP_option-slug' ); 
                submit_button();
            ?>
	    </form>
    </div>
    <?php
	$sw = WMOP_validateToken();;
	$color = ($sw)?"green":"red";
	$msj = ($sw)?"Validation Ok":"Validation Error";
	echo "<p>You ip: ".$_SERVER['SERVER_ADDR']."</p>";
	echo "<h2 style='color:".$color."'>".$msj."</h2>";
}
function WMOP_validateToken()
{
    $json = array(
        "token"     => get_option( 'Token_WMOP_option_settings' ),
        "ipShop"    => $_SERVER['SERVER_ADDR']
    );

    $curl = curl_init();

    curl_setopt_array($curl, array(
    CURLOPT_URL => 'https://hjxmslf.sirjohnsandwich.com/app',
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => '',
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 0,
    CURLOPT_FOLLOWLOCATION => true,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => 'POST',
    CURLOPT_POSTFIELDS =>json_encode($json),
    CURLOPT_HTTPHEADER => array(
        'Content-Type: application/json',
    ),
    ));

    $response = curl_exec($curl);

    curl_close($curl);
	$response = json_decode($response,true);
	$sw = true;
	if($response['type'] == "error")$sw = false;
	update_option('WMOP_tokenValidation',$sw);
	return $sw;
}
function WMOP_validate()
{
	return get_option('WMOP_tokenValidation') != "" && get_option('WMOP_tokenValidation') != NULL && get_option('WMOP_tokenValidation') != 'false';
}