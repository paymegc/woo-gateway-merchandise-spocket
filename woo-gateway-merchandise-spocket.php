<?php
/**
 * Plugin Name: Paymegc Merchandise Gateway Modify
 * Description: Adds a gateway to integrate e-commerce merchandise plugin
 * Version: 1.21.9.30.5
 */

require 'plugin-update-checker/plugin-update-checker.php';
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
	'https://gitlab.com/paymegc/woo-gateway-merchandise-spocket/',
	__FILE__,
	'woo-gateway-merchandise-spocket'
);

//Optional: If you're using a private repository, specify the access token like this:
$myUpdateChecker->setAuthentication('MqiAAZzo9WNBAGzgwc3L');

//Optional: Set the branch that contains the stable release.
$myUpdateChecker->setBranch('master');


if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

require_once plugin_dir_path(__FILE__) . '/OP.php';





if(WMOP_validate()){
	define('WC_MERCHANDISE_API_SHIPPING_METHOD_NAME', "Merchandise Shipping");
	define('WC_MERCHANDISE_API_SHIPPING_METHOD_ID', 639);


	define('WC_MERCHANDISE_API_DIR', plugin_dir_path(__FILE__));
	require_once WC_MERCHANDISE_API_DIR . 'vendor/autoload.php';

	require_once plugin_dir_path(__FILE__) . '/src/includes/WC_Gateway_Merchandise.php';
	
	// activation
	register_activation_hook(__FILE__, array('WC_Gateway_Merchandise', 'activate'));
	// deactivation
	register_deactivation_hook(__FILE__, array('WC_Gateway_Merchandise', 'deactivate'));
	// unisnstall
	register_uninstall_hook(__FILE__, array('WC_Gateway_Merchandise', 'uninstall'));
	
	// initialize plugin
	function action_Init_Merchandise() { 
		return  WC_Gateway_Merchandise::getInstance(__FILE__);
	}
	add_action('woocommerce_init', 'action_Init_Merchandise', 10, 1 );
	
	if (!function_exists('logger')) {
		function logger($log) {WC_Gateway_Merchandise::log($log);}
	}
	add_filter( 'manage_edit-shop_order_columns', 'columns_currency' );
	function columns_currency( $columns ){
		// do something with $columns array
		return array_slice( $columns, 0, 4, true ) // 4 columns before
		+ array( 'currency' => 'Currency' ) // our column is 5th
		+ array_slice( $columns, 4, NULL, true );
	}
	add_filter( 'manage_shop_order_posts_custom_column', 'cutom_columns_currency', 10, 3 );
	function cutom_columns_currency( $colname ) {
		global $the_order; // the global order object
	 
		 if( $colname == 'currency' ) {
			$c_currency = get_post_meta($the_order->get_id() , 'currency_custom',true);
			if(isset($c_currency) && $c_currency!=""){
					echo $c_currency;
				}else{
					echo $the_order->get_currency();
				}
		}
	 
	}
}else{
    function WMOP_log_error() {
        ?>
        <div class="notice notice-error is-dismissible">
            <p>
				You must activate the Token for Merchandise to work perfectly
				<a href="/wp-admin/admin.php?page=WMOP_option-slug">here</a>
            </p>
        </div>
        <?php
    }
    add_action( 'admin_notices', 'WMOP_log_error' );
}
