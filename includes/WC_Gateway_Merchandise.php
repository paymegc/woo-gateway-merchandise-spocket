<?php

class WC_Gateway_Merchandise
{
    public static $plugin_name;
    public static $plugin_path;
    public static $plugin_url;

    function __construct($file)
    {
        if (!$this->validateRequirements()) return null;

        $this->plugin_path  = trailingslashit(plugin_dir_path($file));
        $this->plugin_url   = "";
        
        $this->init();
    }

    /**
     * Initializes WordPress hooks
     */
    public function init() {
        add_filter('plugin_action_links_' . plugin_basename($this->file), [$this, 'setActionLinks']);;

        // Register endpoint for placetopay
        // add_action('rest_api_init', function () {
        //     $self = new GatewayMethod();
        //     $self->logger('register rest route', 'rest_api_init');

        //     register_rest_route($self::PAYMENT_ENDPOINT_NAMESPACE, $self::PAYMENT_ENDPOINT_CALLBACK, [
        //         'methods' => 'POST',
        //         'callback' => [$self, 'endpointPlacetoPay']
        //     ]);
        // }, 1);
    }

    private function initHooks() {
        self::$initiated = true;

        // require_once plugin_dir_path( __FILE__ ) . 'servientrega-shipping-method-class.php';
        // require_once plugin_dir_path( __FILE__ ) . 'servientrega-API-class.php';
        
        // add_action( 'admin_menu', array( 'Servientrega_Plugin', 'add_admin_pages') );
        add_filter( 'plugin_action_links_' . self::get_name() , array( 'WC_Gateway_Merchandise', 'set_action_links') );
        // // add_action( 'wp_enqueue_scripts', array( 'Servientrega_Plugin', 'enqueue_front_assets') );
        // add_filter( 'woocommerce_shipping_methods', array( 'Servientrega_Plugin', 'add_servientrega_shipping_method' ) );
        // add_action( 'woocommerce_order_status_changed', array( 'ServientregaAPI', 'generate_guide' ), 20, 4 );
    }

    /**
     * Method to implement a singleton pattern
     *
     */
    public static function getInstance($file = null)
    {
        if (!(self::$instance instanceof self)) {
            self::$instance = new self($file);
        }

        return self::$instance;
    }

    /**
	 * Attached to activate_{ plugin_basename( __FILES__ ) } by register_activation_hook()
	 * @static
	 */
    public static function activate() {
        
    }

    /**
	 * Attached to deactivate_{ plugin_basename( __FILES__ ) } by register_deactivation_hook()
	 * @static
	 */
    public static function deactivate() {

    }

    /**
	 * Attached to uninstall plugin
	 * @static
	 */
    public static function uninstall() {
        
    }

    public function validateRequirements() {
        if (version_compare(PHP_VERSION, '7.1.0', '<')) {
            if (is_admin() && !defined('DOING_AJAX')) {
                add_action(
                    'admin_notices',
                    function () {
                        self::adminNotice('Servientrega Woocommerce Plugin: plugin fue desarrollado usando PHP 7, algunas funcionalidades podrían fallar en esta versión', 'warning');
                    }
                );
            }
        }

        if (!in_array('woocommerce/woocommerce.php', apply_filters('active_plugins', get_option('active_plugins')), true)) {
            if (is_admin() && !defined('DOING_AJAX')) {
                add_action(
                    'admin_notices',
                    function () {
                        self::adminNotice('Servientrega Woocommerce Plugin: Requiere que se encuentre instalado y activo el plugin: Woocommerce', 'error');
                    }
                );
            }
            return false;
        }

        return true;

    }

    public function setActionLinks($links) {
        $plugin_links = array();
        // settings link
        $plugin_links[] = '<a href="' . admin_url( 'admin.php?page=wc-settings&tab=shipping&section=merchandise_gateway') . '">Settings</a>';
        
        return array_merge( $plugin_links, $links );
    }

    public static function log($msg) {
        if (is_array($msg) || is_object($msg)) $msg = print_r($msg, true);
        $logger = new WC_Logger();
        $logger->add('wc-gateway-merchandise', $msg);
    }

    /**
	 * Display an admin notice
	 */
    public function adminNotice($notice, $type)
    {
        $class = "notice-info";
        if ( $type == "error" ) $class = "notice-error";
        elseif( $type == "warning" ) $class = "notice-warning";
        elseif( $type == "success" ) $class = "notice-success";

        echo "<div class='notice $class'>" .
            '<p>'. esc_html($notice). '</p>' .
        '</div>';
    }
}
