<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

require_once WC_MERCHANDISE_API_DIR . "src/class/CurrencyConverter.php";

$currency_symbol = get_woocommerce_currency_symbol();
$local_cur = get_woocommerce_currency();

if ($localCurrency) {
    $out_cur = $local_cur;
    $rate = 1;
    $converter = new CurrencyConverter($local_cur);
} else {
    $out_cur = $order->get_meta("received_currency");
    $rate = $order->get_meta("currency_conversion_rate");
    $converter = new CurrencyConverter($out_cur);
    $converter->setRate($in_cur, $rate);
}

?>

<html>
<head>
    <meta charset="utf-8">
</head>

<body>
    <div class="invoice-box">
        <table cellpadding="0" cellspacing="0">
            <tr class="top">
                <td colspan="2">
                    <table>
                        <tr>
                            <td class="title">
                                <img src="https://www.sparksuite.com/images/logo.png" style="width:100%; max-width:300px;">
                            </td>
                            
                            <td>
                                Invoice #: <?=$order->get_id()?><br>
                                Created: <?=$order->get_date_completed()?><br>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            
            <tr class="information">
                <td colspan="2">
                    <table>
                        <tr>
                            <td>
                                <?=get_bloginfo('name')?><br>
                                <?=WC()->countries->get_base_address() . ", " . WC()->countries->get_base_address_2()?><br>
                                <?=WC()->countries->get_base_city().", ".WC()->countries->get_base_state()." ".WC()->countries->get_base_postcode()?>
                            </td>
                            
                            <td>
                                <?=$order->get_billing_first_name()." ".$order->get_billing_last_name()?>
                                <?=$order->get_billing_email()?>
                                <?=$order->get_billing_phone()?>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            
            <tr class="heading">
                <td>
                    Item
                </td>
                
                <td>
                    Price (<?=$out_cur?>)
                </td>
            </tr>
            <?php foreach ((array)$order->get_items() as $item_id => $item_data) { 
                $product = $item_data->get_product();
            ?>
            <tr class="item">
                <td>
                    <?=$product->get_name()?> (<?=$item_data->get_quantity()?>)
                </td>
                
                <td>
                    <?=$converter->convert($item_data->get_total(), $in_cur)?>
                </td>
            </tr>
            <?php } ?>
            
            <tr class="item last"></tr>
            
            <tr class="total">
                <td></td>
                
                <td>
                   Subtotal: <?=$converter->convert($order->get_total(), $in_cur)?>
                </td>
            </tr>

            <tr class="heading">
                <td>
                    Payment Method
                </td>
                
                <td>
                    Check #
                </td>
            </tr>
            
            <tr class="details">
                <td>
                    Check
                </td>
                
                <td>
                    1000
                </td>
            </tr>

            <tr class="total">
                <td></td>
                
                <td>
                   Total: <?=$converter->convert($order->get_total(), $in_cur)?>
                </td>
            </tr>
        </table>
    </div>
</body>
</html>