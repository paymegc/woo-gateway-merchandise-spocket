<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

require_once WC_MERCHANDISE_API_DIR . "src/class/CurrencyConverter.php";

$cs = get_woocommerce_currency_symbol();
$in_cur = get_woocommerce_currency();

if ($localCurrency) {
    $out_cur = $in_cur;
    $rate = 1;
    $converter = new CurrencyConverter($out_cur);
} else {
    $out_cur = $order->get_meta("received_currency");
    $rate = $order->get_meta("currency_conversion_rate");
    $converter = new CurrencyConverter($out_cur);
    $converter->setRate($in_cur, $rate);
}

$general_settings = get_option('wpo_wcpdf_settings_general');
logger($order->get_order_item_totals());
?>

<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <title>invoice_<?= $order->get_id() ?></title>
        <!-- <link rel="stylesheet" href="style.css" media="all" /> -->
    </head>
    <body style="display: flex; justify-content: center;">

        <div style="padding: 30px 60px; width: 100%;">

            <div style="display: flex; justify-content: space-between;">
                <div style="display: flex; align-items: center; width: 60%; padding: 0 25px;">
                    <img src="<?=wp_get_attachment_image_src($general_settings['header_logo'],"full")[0]?>" alt="">
                </div>
                <div style="display: flex; flex-direction: column; width: 40%;">
                    <span style="font-size: 16px; font-family: sans-serif; font-weight: bold; line-height: 20px; margin-bottom: 5px;">
                        <?=woocommerce_page_title(); ?>
                    </span>
                    <span style="font-size: 16px; font-family: sans-serif; line-height: 20px;">
                        <?=get_option( 'woocommerce_store_address' ).get_option( 'woocommerce_store_city' ).get_option( 'woocommerce_store_postcode' );?>
                    </span>
                </div>
            </div>

            <div style="padding: 0 25px; margin-bottom: 25px;">
                <h1 style="font-size: 26px; font-family: sans-serif; font-weight: bold; line-height: 20px; margin-bottom: 5px;">
                    INVOICE
                </h1>
            </div>

            <div style="display: flex; justify-content: space-between;">

                <div style="display:flex; flex-direction: column; width: 60%; padding-right: 150px; padding-left: 25px;">
                    <span style="font-size: 16px; font-family: sans-serif; line-height: 25px;">
                        <?= $order->get_formatted_billing_full_name() ?>
                        <?= $order->get_billing_address_1() . ", " .
                            $order->get_billing_address_2() . ", " .
                            $order->get_billing_city() . ", " .
                            $order->get_billing_state() . " " .
                            $order->get_billing_postcode() . ", " .
                            $order->get_billing_country() ?>
                    </span>
                    <span style="font-size: 16px; font-family: sans-serif; line-height: 25px;">
                        <?= $order->get_billing_email() ?>
                    </span>
                </div>
                <div style="display:flex; width: 40%;">
                    <div style="display: flex; flex-direction: column;">
                        <span style="font-size: 16px; font-family: sans-serif; line-height: 25px;">Invoice Number:</span>
                        <span style="font-size: 16px; font-family: sans-serif; line-height: 25px;">Invoice Date:</span>
                        <span style="font-size: 16px; font-family: sans-serif; line-height: 25px;">Currency</span>
                        <span style="font-size: 16px; font-family: sans-serif; line-height: 25px;">Payment Method:</span>
                    </div>
                    <div style="display: flex; flex-direction: column;">
                        <span style="font-size: 16px; font-family: sans-serif; line-height: 25px; margin-left: 7px;"><?= $order->get_id() ?></span>
                        <span style="font-size: 16px; font-family: sans-serif; line-height: 25px; margin-left: 7px;"><?= $order->get_date_completed()->format('Y-m-d') ?></span>
                        <span style="font-size: 16px; font-family: sans-serif; line-height: 25px; margin-left: 7px;"><?= $order->get_currency(); ?></span>
                        <span style="font-size: 16px; font-family: sans-serif; line-height: 25px; margin-left: 7px;">
                        <?php 
                        if($order->get_payment_method_title()==""){
                            $paymentType = get_post_meta($order->get_id(), 'paymentType', true );
                            if($paymentType!=null){
                                switch ($paymentType) {
                                    case 'CC':
                                        echo "Credit Card";
                                        echo "<br>";
                                        echo "<img src='".plugin_dir_url( __FILE__ ) ."img/credit_card.png'/>";
                                        break;
                                    case 'C21':
                                        echo "Check 21";
                                        echo "<br>";
                                        echo "<img src='".plugin_dir_url( __FILE__ ) ."img/c21.svg'/>";
                                        break;
                                    case 'EFT':
                                        echo "EFT Payment Gateway";
                                        break;
                                }
                            }
                        }else{
                            echo $order->get_payment_method_title();
                        }
                        ?>
                        </span>
                    </div>
                </div>

            </div>

            <div style="padding: 0 25px; margin-top: 35px;">
                <table style="width: 100%;">
                    <tr style="background-color: black;">
                        <th style="font-size: 16px; font-family: sans-serif; line-height: 25px; color: #fff; font-weight: bold; width: 60%; padding: 5px 10px; text-align: justify;">
                            Product</th>
                        <th style="font-size: 16px; font-family: sans-serif; line-height: 25px; color: #fff; font-weight: bold; padding: 5px 10px; text-align: justify;">
                            Quantity</th>
                        <th style="font-size: 16px; font-family: sans-serif; line-height: 25px; color: #fff; font-weight: bold; padding: 5px 10px; text-align: justify;">
                            Price</th>
                    </tr>

                    <?php foreach ((array)$order->get_items() as $item_id => $item_data) {
                        $product = $item_data->get_product();
                        logger($item_data->get_total_tax());
                    ?>
                        <tr style="border-bottom: 0.5px solid #ccc;">
                            <td style="font-size: 16px; font-family: sans-serif; line-height: 25px; padding: 5px 10px; text-align: justify;">
                                <?= $product->get_name() ?></td>
                            <td style="font-size: 16px; font-family: sans-serif; line-height: 25px; padding: 5px 10px; text-align: justify;">
                                <?= $item_data->get_quantity() ?></td>
                            <td style="font-size: 16px; font-family: sans-serif; line-height: 25px; padding: 5px 10px; text-align: justify;">
                                <?= $cs . number_format($converter->convert($item_data->get_total(), $in_cur), 2) ?></td>
                        </tr>
                    <?php } ?>
                </table>
            </div>

            <div style="display: flex; justify-content: flex-end; padding: 0 25px; margin-top: 15px;">

                <div style="width: 40%;">
                    <div style="width: 100%; display: flex; border-top: .5px solid #ccc; ">
                        <div style="font-size: 16px; font-family: sans-serif; font-weight: bold; line-height: 25px; width: 50%; padding: 5px 15px;">
                            Subtotal</div>
                        <div style="font-size: 16px; font-family: sans-serif; line-height: 25px; width: 50%; padding: 5px 15px">
                            <?= $cs . number_format($converter->convert($order->get_subtotal(), $in_cur), 2) ?></div>
                    </div>
                    <div style="width: 100%; display: flex; border-top: .5px solid #ccc; ">
                        <div style="font-size: 16px; font-family: sans-serif; font-weight: bold; line-height: 25px; width: 50%; padding: 5px 15px;">
                            Discount</div>
                        <div style="font-size: 16px; font-family: sans-serif; line-height: 25px; width: 50%; padding: 5px 15px">
                            <?= $cs . number_format($converter->convert($order->get_total_discount(), $in_cur), 2) ?></div>
                    </div>
                    <div style="width: 100%; display: flex; border-top: .5px solid #ccc; ">
                        <div style="font-size: 16px; font-family: sans-serif; font-weight: bold; line-height: 25px; width: 50%; padding: 5px 15px;">
                            Shipping</div>
                        <div style="font-size: 16px; font-family: sans-serif; line-height: 25px; width: 50%; padding: 5px 15px">
                            <?= $cs . number_format($converter->convert($order->get_total_shipping(), $in_cur), 2) ?></div>
                    </div>
                    <?php foreach ((array)$order->get_items('fee') as $feed_id => $fee) { ?>
                        <div style="width: 100%; display: flex; border-top: .5px solid #ccc; ">
                            <div style="font-size: 16px; font-family: sans-serif; font-weight: bold; line-height: 25px; width: 50%; padding: 5px 15px;">
                                <?= $fee->get_name() ?></div>
                            <div style="font-size: 16px; font-family: sans-serif; line-height: 25px; width: 50%; padding: 5px 15px">
                                <?= $cs . number_format($converter->convert($fee->get_total(), $in_cur), 2) ?></div>
                        </div>
                    <?php } ?>
                    <div style="width: 100%; display: flex; border-top: 2px solid #000; border-bottom: 2px solid #000;">
                        <div style="font-size: 16px; font-family: sans-serif; font-weight: bold; line-height: 25px; width: 50%; padding: 5px 15px;">
                            Total</div>
                        <div style="font-size: 16px; font-family: sans-serif; line-height: 25px; width: 50%; padding: 5px 15px">
                            <?= $cs . number_format($converter->convert($order->get_total(), $in_cur), 2) ?></div>
                    </div>
                </div>
            </div>
        </div>
    </body>

</html>