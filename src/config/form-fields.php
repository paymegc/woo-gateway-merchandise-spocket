<?php

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

return array(
            'section_title' => array(
                'name'     => __( 'MerchandiseAPI Settings', 'woocommerce-gateway-merchandise' ),
                'type'     => 'title',
                'desc'     => '',
                'id'       => 'wc_merchandise_api_settings'
            ),
            'wc_merchandise_enable' => array(
                'name' => __( 'Enable/Disable', 'woocommerce-gateway-merchandise' ),
                'type' => 'checkbox',
                'label' => __( 'Enable Merchandise API', 'woocommerce-gateway-merchandise' ),
                'id'   => 'wc_merchandise_enable',
                'default' => 'no',
            ),
            'wc_merchandise_user_key' => array(
                'name' => __( 'User Key', 'woocommerce-gateway-merchandise' ),
                'type' => 'text',
                'desc' => __( 'User key pre-generated in Woocommerce API settings', 'woocommerce-gateway-merchandise' ),
                'id'   => 'wc_merchandise_user_key'
            ),
            'wc_merchandise_password_key' => array(
                'name' => __( 'User Key', 'woocommerce-gateway-merchandise' ),
                'type' => 'text',
                'desc' => __( 'Password key pre-generated in Woocommerce API settings', 'woocommerce-gateway-merchandise' ),
                'id'   => 'wc_merchandise_password_key'
            ),
            'wc_merchandise_courier_profle' => array(
                'name' => __('Courier Profile', 'woocommerce-gateway-merchandise' ),
                'type' => 'text',
                'desc' => __('Profile number assigned for the courier service', 'woocommerce-gateway-merchandise' ),
                'id'   => 'wc_merchandise_courier_profle'
            ),
            'wc_merchandise_enable_whitelist' => array(
                'name' => __( 'Enable/Disable Whitelist', 'woocommerce-gateway-merchandise' ),
                'type' => 'checkbox',
                'label' => __( 'Use Whitelist', 'woocommerce-gateway-merchandise' ),
                'id'   => 'wc_merchandise_enable_whitelist',
                'default' => 'no',
            ),
            'wc_whitelist' => array(
                'name' => __('Whitelist', 'woocommerce-gateway-merchandise' ),
                'type' => 'text',
                'desc' => __('List of ip for whitelist, separated by commas (,)' ),
                'id'   => 'wc_whitelist'
            ),
            'section_end' => array(
                'type' => 'sectionend',
                'id' => 'wc_settings_tab_merchandise_api'
            ),
        );