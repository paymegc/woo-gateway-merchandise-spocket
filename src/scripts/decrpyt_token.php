<?php
require_once "../class/JWT.php";
require_once "../includes/WC_Merchandise_Gateway_API.php";

if (!defined('ABSPATH') && $argv[1] != "get") {
    echo "Not authorized!\n";
    exit; // Exit if accessed directly
}

$token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE1OTE1ODkxOTYsImp0aSI6InRva2VuX3ZfMSIsImV4cCI6MTU5NDE4MTE5NiwiZGF0YSI6eyJpcCI6IjM3LjI0Ny4xMTYuNTEifX0.d8JEzXiDc0QL64yYEYFUPpC9g8wI-Iw3snftZdqwwDE";

$decodedToken = WC_Merchandise_Gateway_API::decodeToken($token);

print_r($decodedToken);
exit;
