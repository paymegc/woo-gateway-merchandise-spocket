<?php

require_once "../class/JWT.php";
require_once "../includes/WC_Merchandise_Gateway_API.php";

if (!defined('ABSPATH') && $argv[1] != "get") {
    echo "Not authorized!\n";
    exit; // Exit if accessed directly
}

$data["ip"] = "127.0.0.1";

$token = WC_Merchandise_Gateway_API::generateToken($data);

echo "{$token}\n";
exit;
