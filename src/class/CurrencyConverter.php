<?php
/**
 * Note: 
 * - This class uses exchange rates API published by the European Central Bank (https://exchangeratesapi.io/)
 */
class CurrencyConverter
{
    const API_URL = "https://api.exchangeratesapi.io";
    const LATEST_ROUTE = "/latest";
    const HISTORY_ROUTE = "/history";

    private $base_currency;
    private $rates;

    /**
     * Class constructor
     *
     * @param string $base_currency     local currency (base)
     */
    function __construct($base_currency)
    {
        $this->base_currency = $base_currency;
        $this->rates = array();

        if ($base_currency) $this->rates[$base_currency] = array();
    }

    /**
     * Covnert value from one currency to another
     *
     * @param float $value      value to convert
     * @param string $in_cur    currency base to make conversion
     * @param string $out_cur   local Currency
     * @param boolean $cache    catch results
     * @return void
     */
    public function convert($value, $in_cur, $out_cur = false, $cache = true) {
        
        $out_cur = $out_cur ? $out_cur : $this->base_currency;
        
        if (!$out_cur) throw new Exception("Missing base currency", 1);
    
        // return same value if out_cur and in_cur are equal
        if ($out_cur == $in_cur) return $value;

        // get conversion rate
        $rate = self::getRate($in_cur, $out_cur, $cache);
        
        if (!$rate) throw new Exception("Unsupported currency: {$in_cur}", 1);
        
        $out_value = round($value*$rate, 2);
        
        return $out_value;
    }

    /**
     * Get conversion rate
     *
     * @param string $in_cur    currency base to make conversion
     * @param string $out_cur   local Currency
     * @param boolean $cache    catch results
     * @return float            conversion rate    
     */
    public function getRate($in_cur, $out_cur = false, $cache = true) {

        $out_cur = $out_cur ? $out_cur : $this->base_currency;
        
        if (!isset($this->rates[$out_cur])) $this->rates[$out_cur] = array();

        if ($cache && isset($this->rates[$out_cur][$in_cur])) return $this->rates[$out_cur][$in_cur];

        $params = array(
            "base" => $in_cur,
            "symbols" => $out_cur,
        );
        
        $res = CurrencyConverter::apiCall(self::get_url(), "GET", $params);
        
        $out = $res["rates"][$out_cur] ? floatval($res["rates"][$out_cur]) : 0;

        $this->rates[$out_cur][$in_cur] = $out;

        return $this->rates[$out_cur][$in_cur];
    }

    /**
     * Set a conversion rate to avoid using the API
     *
     * @param string $in_cur    currency base to make conversion (desired currecy)
     * @param string $rate
     * @return void
     */
    public function setRate($in_cur, $rate) {
        $out_cur = $out_cur ? $out_cur : $this->base_currency;

        if (!isset($this->rates[$out_cur])) $this->rates[$out_cur] = array();

        if (empty($in_cur)) return;
        
        $this->rates[$out_cur][$in_cur] = $rate;
    }

    /**
     * Check if a currency is convertible according to currencies supported by European Central Bank API
     *
     * @param string $cur   currency to convert
     * @return boolean
     */
    static public function isConvertible($cur) {
        $currencies = array(
            "EUR",
            "CAD",
            "HKD",
            "ISK",
            "PHP",
            "DKK",
            "HUF",
            "CZK",
            "AUD",
            "RON",
            "SEK",
            "IDR",
            "INR",
            "BRL",
            "RUB",
            "HRK",
            "JPY",
            "THB",
            "CHF",
            "SGD",
            "PLN",
            "BGN",
            "TRY",
            "CNY",
            "NOK",
            "NZD",
            "ZAR",
            "USD",
            "MXN",
            "ILS",
            "GBP",
            "KRW",
            "MYR",
        );
        
        if (in_array($cur, $currencies)) return true;
        
        return false;
    }

    /**
     * Useful function to make HTTP requests
     *
     * @param string $url       service location
     * @param string $method    HTTP method
     * @param array $params     Request body when method is POST, request params when GET
     * @return void             HTTP response
     */
    static private function apiCall($url, $method = "GET", $params = array()) {
        // initialize cURL.
        $ch = curl_init();
        // set request parameters
        if ($method == "GET" && $params) {
            $params = http_build_query($params);
            $url = $url."?".$params;
        }
        // set the URL that you want to GET by using the CURLOPT_URL option.
        curl_setopt($ch, CURLOPT_URL, $url);
        // set CURLOPT_RETURNTRANSFER so that the content is returned as a variable.
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // execute the request.
        $data = curl_exec($ch);
        // close the cURL handle.
        curl_close($ch);
        
        if (gettype($data) === "string") {
            $data = json_decode($data, true);
        }

        return $data;
    }

    /**
     * Wrap to create API URL 
     *
     * @param string $route
     * @return string API URL
     */
    static function get_url($route = "latest") {
        if ($route == "history") return CurrencyConverter::API_URL . CurrencyConverter::HISTORY_ROUTE;
        else return CurrencyConverter::API_URL . CurrencyConverter::LATEST_ROUTE;
    }
}
