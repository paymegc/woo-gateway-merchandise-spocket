<?php
/**
 * WC_Merchandise_getProduct
 * @description funcion para obtener una Product por medio de ID
 * @param req
 * @return product
 */
function WC_Merchandise_getProduct($req){
    $ID = $req->get_param("ID");
    if(!$ID){
        return array(
            "type"=>"error",
            "msj"=>"ID requeride"
        );
    }
    $product = wc_get_product($ID);
    $product_data = $product->get_data();
    if($product_data['id'] == 0){
        return array(
            "type"=>"error",
            "msj"=>"ID Invalid"
        );
    }
    return array(
        "type"=>"ok",
        "product"=>$product_data
    );
}

/**
 * WC_Merchandise_getProducts
 * @description funcion para obtener all ID Product
 * @param req
 * @return product_id email
 */
function WC_Merchandise_getProducts($req){
    $products_ids = get_posts( array(
        'post_type' => 'product',
        'numberposts' => -1,
        'fields' => 'ids',
     ) );
    $products = [];
    if($req->get_param("all") == "1"){
        $general_settings = get_option('WOOCS');
        $currency = $req->get_param("currency");
        $currency = strtoupper($currency);
        if(!array_key_exists($currency,$general_settings)) {
            return array(
                "type"=>"error",
                "msj"=>"currency not exist"
            );
        }

        $rate               = $general_settings[$currency]["rate"];
        $conversion_rate    = $general_settings[$currency]["rate"];

        for ($i=0; $i < count($products_ids); $i++) {
            $product_id = $products_ids[$i];
            $product = wc_get_product( $product_id ); 
            $products[] = $product->get_data();
            $products[$i] = json_decode(json_encode($products[$i]),true);
            $products[$i]["price"] = floatval($products[$i]["price"]) * $rate;
            $products[$i]["regular_price"] = floatval($products[$i]["regular_price"]) * $rate;
            $products[$i]["sale_price"] = floatval($products[$i]["sale_price"]) * $rate;
        }
    }else{
        $products = $products_ids;
    }

    return array(
        "type"=>"ok",
        "products"=>$products
    );
}