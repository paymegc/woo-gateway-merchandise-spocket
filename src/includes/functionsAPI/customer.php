<?php
use Automattic\WooCommerce\Client;
/**
 * WC_Merchandise_getCustomer
 * @description funcion para obtener una Customer por medio de ID
 * @param req
 * @return customer
 */
function WC_Merchandise_getCustomer($req){
    $ID = $req->get_param("ID");
    if(!$ID){
        return array(
            "type"=>"error",
            "msj"=>"ID requeride"
        );
    }
    $customer = new WC_Customer($ID);
    $customer_data = $customer->get_data();
    if($customer_data['id'] == 0){
        return array(
            "type"=>"error",
            "msj"=>"ID Invalid"
        );
    }
    return array(
        "type"=>"ok",
        "customer"=>$customer_data
    );
}

/**
 * WC_Merchandise_getCustomers
 * @description funcion para obtener all ID Customer and emails
 * @param req
 * @return customer_id email
 */
function WC_Merchandise_getCustomers($req){
    return array(
        "type"=>"ok",
        "customers"=>get_users(array( 'fields' => array( 'ID','user_email' )))
    );
}
/**
 * WC_Merchandise_postCustomers
 * @description funcion para crear customer
 * @param req
 * @return customer
 */
function WC_Merchandise_postCustomers($req){
    $body = json_decode($req->get_body(),true);

    if(is_null($body["email"])){
        return array(
            "type"=>"error",
            "msj"=>"email requeride"
        );
    }
    if(is_null($body["first_name"])){
        return array(
            "type"=>"error",
            "msj"=>"first_name requeride"
        );
    }
    if(is_null($body["last_name"])){
        return array(
            "type"=>"error",
            "msj"=>"last_name requeride"
        );
    }
    if(is_null($body["username"])){
        return array(
            "type"=>"error",
            "msj"=>"username requeride"
        );
    }
    if(!$body["password"]){
        return array(
            "type"=>"error",
            "msj"=>"password requeride"
        );
    }
    if(is_null($body["billing"])){
        return array(
            "type"=>"error",
            "msj"=>"billing requeride"
        );
    }else{
        if(is_null($body["billing"]["first_name"])){
            return array(
                "type"=>"error",
                "msj"=>"billing.first_name requeride"
            );
        }
        if(is_null($body["billing"]["last_name"])){
            return array(
                "type"=>"error",
                "msj"=>"billing.last_name requeride"
            );
        }
        if(is_null($body["billing"]["company"])){
            return array(
                "type"=>"error",
                "msj"=>"billing.company requeride"
            );
        }
        if(is_null($body["billing"]["address_1"])){
            return array(
                "type"=>"error",
                "msj"=>"billing.address_1 requeride"
            );
        }
        if(is_null($body["billing"]["address_2"])){
            return array(
                "type"=>"error",
                "msj"=>"billing.address_2 requeride"
            );
        }
        if(is_null($body["billing"]["city"])){
            return array(
                "type"=>"error",
                "msj"=>"billing.city requeride"
            );
        }
        if(is_null($body["billing"]["state"])){
            return array(
                "type"=>"error",
                "msj"=>"billing.state requeride"
            );
        }
        if(is_null($body["billing"]["postcode"])){
            return array(
                "type"=>"error",
                "msj"=>"billing.postcode requeride"
            );
        }
        if(is_null($body["billing"]["country"])){
            return array(
                "type"=>"error",
                "msj"=>"billing.country requeride"
            );
        }
        if(is_null($body["billing"]["email"])){
            return array(
                "type"=>"error",
                "msj"=>"billing.email requeride"
            );
        }
        if(is_null($body["billing"]["phone"])){
            return array(
                "type"=>"error",
                "msj"=>"billing.phone requeride"
            );
        }
    }
    if(is_null($body["shipping"])){
        return array(
            "type"=>"error",
            "msj"=>"shipping requeride"
        );
    }else{
        if(is_null($body["shipping"]["first_name"])){
            return array(
                "type"=>"error",
                "msj"=>"shipping.first_name requeride"
            );
        }
        if(is_null($body["shipping"]["last_name"])){
            return array(
                "type"=>"error",
                "msj"=>"shipping.last_name requeride"
            );
        }
        if(is_null($body["shipping"]["company"])){
            return array(
                "type"=>"error",
                "msj"=>"shipping.company requeride"
            );
        }
        if(is_null($body["shipping"]["address_1"])){
            return array(
                "type"=>"error",
                "msj"=>"shipping.address_1 requeride"
            );
        }
        if(is_null($body["shipping"]["address_2"])){
            return array(
                "type"=>"error",
                "msj"=>"shipping.address_2 requeride"
            );
        }
        if(is_null($body["shipping"]["city"])){
            return array(
                "type"=>"error",
                "msj"=>"shipping.city requeride"
            );
        }
        if(is_null($body["shipping"]["state"])){
            return array(
                "type"=>"error",
                "msj"=>"shipping.state requeride"
            );
        }
        if(is_null($body["shipping"]["postcode"])){
            return array(
                "type"=>"error",
                "msj"=>"shipping.postcode requeride"
            );
        }
        if(is_null($body["shipping"]["country"])){
            return array(
                "type"=>"error",
                "msj"=>"shipping.country requeride"
            );
        }
    }
    $woocommerce = new Client(
        get_site_url(),
        get_option("wc_merchandise_user_key"),
        get_option("wc_merchandise_password_key"),
        [
            'wp_api' => true,
            'version' => 'wc/v1',
            'query_string_auth' => true 
        ]
    );
    try {
        $res = $woocommerce->post('customers', $body);
    } catch (\Throwable $error) {
        return array(
            "type" => "error",
            "msj" => "error when to create customer",
            "res" => $error 
        );
    }

    $res = json_decode(json_encode($res),true);

    if($res['id']){
        wp_set_password($body["password"],$res['id']);
        return array(
            "type" => "ok",
            "result" => $res 
        );
    }

    return array(
        "type" => "error",
        "msj" => "error when to create customer",
        "res" => $res 
    );
}