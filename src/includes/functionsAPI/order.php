<?php
use Automattic\WooCommerce\Client;
/**
 * WC_Merchandise_getOrder
 * @description funcion para obtener una Order por medio de ID
 * @param req
 * @return order
 */
function WC_Merchandise_getOrder($req){
    $ID = $req->get_param("ID");
    if(!$ID){
        return array(
            "type"=>"error",
            "msj"=>"ID requeride"
        );
    }
    $order = wc_get_order( $ID );
    if(!$order){
        return array(
            "type"=>"error",
            "msj"=>"ID Invalid"
        );
    }
    $order_data = $order->get_data();
    return array(
        "type"=>"ok",
        "order"=>$order_data
    );
}

/**
 * WC_Merchandise_getOrders
 * @description funcion para obtener all ID Order
 * @param req
 * @return orders_id
 */
function WC_Merchandise_getOrders($req){
    global $wpdb;
    $orders_statuses = $req->get_param("orders_statuses");
    $query = "
        SELECT DISTINCT woi.order_id
        FROM {$wpdb->prefix}woocommerce_order_itemmeta as woim, 
            {$wpdb->prefix}woocommerce_order_items as woi, 
            {$wpdb->prefix}posts as p
        WHERE  woi.order_item_id = woim.order_item_id
        AND woi.order_id = p.ID
        
    ";
    if($orders_statuses){
        $query .= " AND p.post_status IN ( $orders_statuses )" ;
    }
    return array(
        "type"=>"ok",
        "orders_ids"=>$wpdb->get_col($query)
    );
}
/**
 * WC_Merchandise_postOrder
 * @description funcion para crear una orden
 * @param req
 * @return orders_id
 */
function WC_Merchandise_postOrder($req){
    $general_settings = get_option('WOOCS');
    $body = json_decode($req->get_body(),true);

    if(!$body["customerId"]){
        return array(
            "type"=>"error",
            "msj"=>"customerId requeride"
        );
    }
    if(!$body["currency"]){
        return array(
            "type"=>"error",
            "msj"=>"currency requeride"
        );
    }
    if(!$body["paymentType"]){
        return array(
            "type"=>"error",
            "msj"=>"paymentType requeride"
        );
    }
    if(!$body["products"]){
        return array(
            "type"=>"error",
            "msj"=>"products requeride"
        );
    }


    //exist currency
    $body["currency"] = strtoupper($body["currency"]);
    if (!array_key_exists($body["currency"],$general_settings)) {
        return array(
            "type"=>"error",
            "msj"=>"currency not exist"
        );
    }

    $rate               = $general_settings[$body["currency"]]["rate"];
    $conversion_rate    = $general_settings[$body["currency"]]["rate"];

    //validate customer
    $customer = new WC_Customer($body["customerId"]);
    $customer_data = $customer->get_data();
    if($customer_data['id'] == 0){
        return array(
            "type"=>"error",
            "msj"=>"customerId Invalid"
        );
    }
    //country
    $country = $customer->get_billing_country();
    switch ($country) {
        case 'US':
            $country = "United States (US)";
            break;
        case 'CA':
            $country = "Canada (CA)";
            break;
        default:
            return array(
                "type"=>"error",
                "msj"=>"country Invalid"
            );
            break;
    }

    //validate Payment
    switch ($body["paymentType"]) {
        case 'CC':
            $paymentType_id = 'woo-tkp-cc-gateway';
            $paymentType_title = 'Credit Card';
            break;
        case 'C21':
            $paymentType_id = 'woo-tkp-c21-gateway';
            $paymentType_title = 'Check21';
            break;
        case 'EFT':
            $paymentType_id = 'woo-tkp-eft-gateway';
            $paymentType_title = 'EFT Echeck';
            break;
        default:
            return array(
                "type"=>"error",
                "msj"=>"paymentType Invalid"
            );
            break;
    }

    //validate shipping_tax
    $body["shipping_tax"] = $body["shipping_tax"]? $body["shipping_tax"] : 0;

    $data = [
        'payment_method' => $paymentType_id,
        'payment_method_title' => $paymentType_title,
        "status" => "pending",
        'set_paid' => false,
        'currency' => $body["currency"],
        'rate'     => $rate,
        "customer_id" => $body["customerId"],
        'billing' => [
            "first_name"    => $customer->get_billing_first_name(),
            "last_name"     => $customer->get_billing_last_name(),
            "address_1"     => $customer->get_billing_address_1(),
            "address_2"     => $customer->get_billing_address_2(),
            "city"          => $customer->get_billing_city(),
            "state"         => $customer->get_billing_state(),
            "postcode"      => $customer->get_billing_postcode(),
            "country"       => $country,
            "email"         => $customer->get_billing_email(),
            "phone"         => $customer->get_billing_phone(),
        ],
        'shipping' => [
            "first_name"    => $customer->get_billing_first_name(),
            "last_name"     => $customer->get_billing_last_name(),
            "address_1"     => $customer->get_billing_address_1(),
            "address_2"     => $customer->get_billing_address_2(),
            "city"          => $customer->get_billing_city(),
            "state"         => $customer->get_billing_state(),
            "postcode"      => $customer->get_billing_postcode(),
            'country'       => $country
        ],
        "meta_data" => array(
            array(
                "key" => "no_email",
                "value" => 1,
            ),
            array(
                "key" => "merchandise_api",
                "value" => 1,
            ),
            array(
                "key" => "received_currency",
                "value" => $body["currency"],
            ),
            array(
                "key" => "rate",
                "value" => $rate,
            ),
            array(
                "key" => "currency_conversion_rate",
                "value" => $conversion_rate,
            ),
            array(
                "key" => "shipping_tax",
                "value" => $body["shipping_tax"]? $body["shipping_tax"] : 0,
            )
        ),
        "line_items" => array(),
    ];
    
    $data["shipping_lines"][] = array(
        "method_title"  => WC_MERCHANDISE_API_SHIPPING_METHOD_NAME,
        "method_id"     => WC_MERCHANDISE_API_SHIPPING_METHOD_ID,
        "total"         => number_format($body["shipping_tax"], 2),
    );


    //products
    foreach ($body["products"] as $key => $product) {
        if(!$product['id']){
            return array(
                "type"=>"error",
                "msj"=>"product id requeride in $key"
            );
        }
        if(!$product['quantity']){
            return array(
                "type"=>"error",
                "msj"=>"product quantity requeride in $key"
            );
        }
        if($product['quantity'] < 1){
            return array(
                "type"=>"error",
                "msj"=>"product quantity invalid in $key"
            );
        }
        $product['salesTax'] = $product['salesTax'] ? $product['salesTax'] : 0 ;
        $product['applyDiscount'] = $product['applyDiscount'] ? $product['applyDiscount'] : 0 ;

        $_product = wc_get_product( $product['id'] );
        
        if (!$_product){
            return array(
                "type"=>"error",
                "msj"=>"product id invalid in ".$product['id']
            );
        }

        $product_name   = $_product->get_name();
        
        $subtotal       = round(($_product->get_price( $in_cur)*$rate), 2)*$product['quantity'] ; # before discount
        $total          = $subtotal - $product['applyDiscount'];
        
        $data["line_items"][] = array(
            "product_id"    => $product['id'],
            "quantity"      => $product['quantity'],
            "subtotal"      => number_format($subtotal, 2),
            "total"         => number_format($total, 2),
            "meta_data"     => array(
                array(
                    "key" => "sales_tax",
                    "value" => $product['salesTax']
                ),
                array(
                    "key" => "discount",
                    "value" => $product['applyDiscount']
                )
            ),
        );

        // add product taxes
        if ($product['salesTax'] != 0) {   
            $data["fee_lines"][] = array(
                "name" => "Sales tax - {$product_name} (x{".$product['quantity']."})",
                "tax_status" => "taxable",
                "total" => number_format($product['salesTax'], 2),
            );
        }
    }
    
    $url = get_site_url()."/wp-json/wc/v1/orders";
    // $res = WC_Merchandise_Request($url,"POST", $data);
    $woocommerce = new Client(
        get_site_url(),
        get_option("wc_merchandise_user_key"),
        get_option("wc_merchandise_password_key"),
        [
            'wp_api' => true,
            'version' => 'wc/v1',
            'query_string_auth' => true 
        ]
    );
    $res = $woocommerce->post('orders', $data);

    $res = json_decode(json_encode($res),true);

    if ($res["id"]) {
        // update order status
        $order = wc_get_order($res["id"]);
        
        update_post_meta($res["id"] , 'no_email', 1);
        update_post_meta($res["id"] , 'referenceId', $body["referenceId"]);
        update_post_meta($res["id"] , 'paymentType', $body["paymentType"]);
        update_post_meta($res["id"] , 'line_items', $data["line_items"]);

        $order->update_status('processing');
        $order->update_status('completed');

        // get order invoice
        $invoice        = wcpdf_get_invoice($res["id"]);
        $invoice_number = $invoice->get_number();

        update_post_meta($res["id"] , 'invoiceId', $invoice_number->formatted_number);
        update_post_meta($res["id"] , 'invoiceDate', date("F d, Y"));
        
        return array(
            "type" => "ok",
            "result" => array(
                "orderId" => $res["id"],
                "invoiceId" => $invoice_number->formatted_number,
                'total' => $order->get_total(),
            )
        );
    }
    return array(
        "type"=>"error",
        "msj"=>"error in request for create order",
        "res"=>$res
    );
    return  $data;
}


/**
 * WC_Merchandise_putOrder
 * @description funcion para actualizar una orden
 * @param req
 * @return orders_id
 */
function WC_Merchandise_putOrder($req){
    $body = json_decode($req->get_body(),true);

    if(!$body['order_id']){
        return array(
            "type"=>"error",
            "msj"=>"order_id requeride"
        );
    }
    if(!$body['status']){
        return array(
            "type"=>"error",
            "msj"=>"status requeride"
        );
    }
    $order = wc_get_order( $body['order_id'] );

    if(!$order){
        return array(
            "type"=>"error",
            "msj"=>"order_id invalid"
        );
    }
    
    $status = ["wc-completed","wc-processing","wc-pending","wc-on-hold","wc-refunded","wc-cancelled","wc-failed"];

    if(!in_array($body['status'],$status)){
        return array(
            "type"=>"error",
            "msj"=>"status invalid",
            "status_ok"=>$status
        );
    }

    $order->update_status($body['status']);
    
    return array(
        "type"=>"ok",
        "msj"=>"order update"
    );
}