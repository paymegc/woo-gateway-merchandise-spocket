<?php
require_once WC_MERCHANDISE_API_DIR . 'src/includes/dompdf/autoload.inc.php';

use Spipu\Html2Pdf\Html2Pdf;
use Spipu\Html2Pdf\Exception\Html2PdfException;
use Spipu\Html2Pdf\Exception\ExceptionFormatter;

function WC_Merchandise_getInvoiceHTMLTemplate($order_id, $order, $localCurrency) {
    ob_start();
    $general_settings = get_option('wpo_wcpdf_settings_general');

    $cs = get_woocommerce_currency_symbol();
    $in_cur = get_woocommerce_currency();
    
    $invoiceId = get_post_meta($order_id , 'invoiceId', true);
    $invoiceDate = get_post_meta($order_id  , 'invoiceDate', true);
    if ($localCurrency) {
        $out_cur = $in_cur;
        $rate = 1;
    } else {
        $out_cur = $order->get_meta("received_currency");
        $rate = $order->get_meta("currency_conversion_rate");
    }
    
    ?>
    <html>
        <head>
            <meta charset="utf-8">
            <title>invoice_<?=$order_id?></title>
            <style>
                table{
                    width:1000px;
                }
                .logo{
                    max-width:100px;
                }
                .panel{
                    background:black;
                    color:white;
                }
                td{
                    vertical-align: baseline;
                }
                .border-bottom-1{
                    border-bottom:1px solid black;
                }
                <?php
                    if($general_settings['css_custom_1']){
                        if($general_settings['css_custom_1']['default']){
                            echo $general_settings['css_custom_1']['default'];
                        }
                    }
                ?>
            </style>
        </head>
        <body>
            <table>
                <tbody>
                    <tr>
                        <td colspan="3">
                        <?php
                            if(array_key_exists('header_logo',$general_settings)){
                                $url = wp_get_attachment_image_src($general_settings['header_logo'], 'full')[0];
                                ?>
                                <img src="<?=$url?>" class="logo"/>
                                <?php
                            }
                        ?>
                        </td>
                        <td>
                            <h1>
                                <?php 
                                    if(array_key_exists('shop_name',$general_settings)){
                                        echo $general_settings['shop_name']["default"]; 
                                        
                                    }
                                ?>
                            </h1>
                            <?php 
                                if(array_key_exists('shop_address',$general_settings)){
                                    echo $general_settings['shop_address']["default"]; 
                                    
                                }
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" >
                            <h1>Invoice</h1>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td colspan="2">
                                        <h4>Billing Address</h4>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <strong>
                                            Name
                                        </strong>
                                    </td>
                                    <td>
                                        <?=$order->get_billing_first_name()." ".$order->get_billing_last_name()?>
                                    </td>
                                </tr>
                                <?php
                                    if($order->get_billing_company()){
                                        ?>
                                        <tr>
                                            <td>
                                                <strong>

                                                </strong>
                                            </td>
                                            <td>
                                                <?=$order->get_billing_company()?>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                ?>
                                <tr>
                                    <td>
                                        <strong>
                                            Address
                                        </strong>
                                    </td>
                                    <td>
                                        <?=$order->get_billing_address_1()."<br>".$order->get_billing_address_2()?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <strong>
                                            City
                                        </strong>
                                    </td>
                                    <td>
                                        <?=$order->get_billing_city()?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <strong>
                                            State
                                        </strong>
                                    </td>
                                    <td>
                                        <?=$order->get_billing_state()?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <strong>
                                            PostCode
                                        </strong>
                                    </td>
                                    <td>
                                        <?=$order->get_billing_postcode()?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <strong>
                                            Country
                                        </strong>
                                    </td>
                                    <td>
                                        <?=$order->get_billing_country()?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <strong>
                                            Email
                                        </strong>
                                    </td>
                                    <td>
                                        <?=$order->get_billing_email()?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <strong>
                                            Phone
                                        </strong>
                                    </td>
                                    <td>
                                        <?=$order->get_billing_phone()?>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td>
                            <table>
                                <tr>
                                    <td colspan="2">
                                        <h4>Shipping Address</h4>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <strong>
                                            Name
                                        </strong>
                                    </td>
                                    <td>
                                        <?=$order->get_shipping_first_name()." ".$order->get_shipping_last_name()?>
                                    </td>
                                </tr>
                                <?php
                                    if($order->get_shipping_company()){
                                        ?>
                                        <tr>
                                            <td>
                                                <strong>

                                                </strong>
                                            </td>
                                            <td>
                                                <?=$order->get_shipping_company()?>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                ?>
                                <tr>
                                    <td>
                                        <strong>
                                            Address
                                        </strong>
                                    </td>
                                    <td>
                                        <?=$order->get_shipping_address_1()."<br>".$order->get_shipping_address_2()?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <strong>
                                            City
                                        </strong>
                                    </td>
                                    <td>
                                        <?=$order->get_shipping_city()?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <strong>
                                            State
                                        </strong>
                                    </td>
                                    <td>
                                        <?=$order->get_shipping_state()?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <strong>
                                            PostCode
                                        </strong>
                                    </td>
                                    <td>
                                        <?=$order->get_shipping_postcode()?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <strong>
                                            Country
                                        </strong>
                                    </td>
                                    <td>
                                        <?=$order->get_shipping_country()?>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td colspan="2">
                            <table>
                                <tr>
                                    <td colspan="2">
                                        <h4>
                                            Invoice
                                        </h4>
                                    </td>
                                </tr>
                                <tr class="invoice-number">
                                    <td><strong>Invoice Number:</strong></td>
                                    <td><?php echo $order_id; ?></td>
                                </tr>
                                <tr class="invoice-date">
                                    <td><strong>Invoice Date:</strong></td>
                                    <td><?php echo $invoiceDate; ?></td>
                                </tr>
                                <tr class="currency">
                                    <td><strong>Currency:</strong></td>
                                    <td><?= $order->get_currency(); ?></td>
                                </tr>
                                <tr class="payment-method">
                                    <td><strong>Payment Method:</strong></td>
                                    <td>
                                        <?=$order->get_payment_method_title();?>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr class="panel">
                        <td colspan="2">
                            Products
                        </td>
                        <td>
                            Quantity
                        </td>
                        <td>
                            Price
                        </td>
                    </tr>
                    <?php
                        foreach ( $order->get_items() as $item_id => $item ) {
                            $product_id = $item->get_product_id();
                            $variation_id = $item->get_variation_id();
                            $product = $item->get_product();
                            $name = $item->get_name();
                            $quantity = $item->get_quantity();
                            $subtotal = $item->get_subtotal();
                            $total = $item->get_total();
                            $tax = $item->get_subtotal_tax();
                            $taxclass = $item->get_tax_class();
                            $taxstat = $item->get_tax_status();
                            $allmeta = $item->get_meta_data();
                            $sales_tax = $item->get_meta( 'sales_tax', true );
                            $type = $item->get_type();
                            $product = wc_get_product( $product_id );
                            ?>
                            <tr class="border-bottom-1">
                                <td colspan="2" class="border-bottom-1">
                                    <?=$name?>
                                    <br>
                                    SKU: <?=$product->get_sku();?>
                                    <br>
                                    Weight: <?=$product->get_weight();?>kg
                                </td>
                                <td class="border-bottom-1">
                                    <?=$quantity?>
                                </td>
                                <td class="border-bottom-1">
                                    <?=$subtotal?>
                                </td>
                            </tr>
                            <?php
                        }
                    ?>
                    <tr>
                        <td colspan="2"></td>
                        <td class="border-bottom-1">
                            <strong>
                                Subtotal
                            </strong>
                        </td>
                        <td class="border-bottom-1">
                            <?=$order->get_subtotal()?>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2"></td>
                        <td class="border-bottom-1">
                            <strong>
                                Discount
                            </strong>
                        </td>
                        <td class="border-bottom-1">
                            <?=$order->get_discount_total()?>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2"></td>
                        <td class="border-bottom-1">
                            <strong>
                                Shipping
                            </strong>
                        </td>
                        <td class="border-bottom-1">
                            <?=$order->get_shipping_total()?>
                        </td>
                    </tr>
                    <?php
                    $tax = get_post_meta($order_id , 'line_items', true);
                    if($tax != null){
                        for ($i=0; $i < count($tax); $i++) { 
                            $product_id = $tax[$i]['product_id'];
                            $product = wc_get_product( $product_id );
                            ?>
                            <tr>
                                <td colspan="2"></td>
                                <td class="border-bottom-1">
                                    <strong>
                                        Sales tax - <?=$product->get_name();?> <?=($tax[$i]['quantity'] == 1)?"":"(x".$tax[$i]['quantity'].")"?>
                                    </strong>
                                </td>
                                <td class="border-bottom-1">
                                    <?=$tax[$i]['meta_data'][0]["value"]?>
                                </td>
                            </tr>
                            <?php
                        }
                    }
                    ?>
                    <tr>
                        <td colspan="2"></td>
                        <td class="border-bottom-1">
                            <strong>
                                Total
                            </strong>
                        </td>
                        <td class="border-bottom-1">
                            <?=$order->get_total()?>
                        </td>
                    </tr>
                </tbody>
            </table>
        </body>
    </html>
    <?php
    $html = ob_get_clean();
    
    return $html;
}
/**
 * WC_Merchandise_getInvoice
 * @description funcion para obtener una Order por medio de ID y retornar el invoice
 * @param req
 * @return order
 */
function WC_Merchandise_getInvoice($req){
    $ID = $req->get_param("ID");
    $localCurrency = $req->get_param("localCurrency") ? true : false;
    if(!$ID){
        return array(
            "type"=>"error",
            "msj"=>"ID requeride"
        );
    }
    $order = wc_get_order( $ID );
    if(!$order){
        return array(
            "type"=>"error",
            "msj"=>"ID Invalid"
        );
    }

    $wc_merchandise_invoice = array(
        'url'		=> wp_nonce_url( admin_url( "admin-ajax.php?action=generate_wpo_wcpdf&order_ids=" .$ID  ), 'generate_wpo_wcpdf' ),
        'img'		=> $icon,
        'alt'		=> "invoice_" . $ID ,
    );
    apply_filters( 'wpo_wc_merchandise_invoice', $wc_merchandise_invoice );	
    return array(
        "type"=>"ok",
        "invoice_url" => admin_url( "admin-ajax.php?action=generate_wpo_wcpdf&order_ids=" .$ID  ),
    );
}

/**
 * WC_Merchandise_getInvoicePDF
 * @description converte html to pdf
 * @param html
 * @return pdf
 */
function WC_Merchandise_HTML_to_PDF($ID,$html){
    $html2pdf = new Html2Pdf();
    $html2pdf->writeHTML($html);
    return $html2pdf->output("invoice_$ID.pdf");
}
function WC_Merchandise_getInvoice_PDF(){
    if($_GET['order']){
        $order = wc_get_order( $_GET['order'] );
        if(!$order){
            return array(
                "type"=>"error",
                "msj"=>"ID Invalid"
            );
        }
        // header("Content-Type: application/octet-stream");
        // header("Content-Type: application/pdf");
        $html = WC_Merchandise_getInvoiceHTMLTemplate( $_GET['order'],$order,false);
        return WC_Merchandise_HTML_to_PDF($_GET['order'],$html);
    }else{
        return array(
            "type"=>"error",
            "msj"=>"ID Invalid"
        );
    }
}