<?php

class WC_Gateway_Merchandise
{
    const PLUGIN_SETTINGS_KEY = 'wc_gateway_merchandise_settings';

    public $plugin_name;
    private static $instance = null;

    public function __construct($file)
    {
        if (!$this->validateRequirements()) {
            return null;
        }

        $this->plugin_name = plugin_basename($file);
        
        $this->init();
    }

    /**
     * Initializes WordPress hooks
     */
    public function init()
    {
        require_once plugin_dir_path( __FILE__ ) . "WC_Merchandise_Gateway_API.php";
        $merchandiseAPI = new WC_Merchandise_Gateway_API();

        // set admin settings
        add_filter("plugin_action_links_{$this->plugin_name}", array($this, 'setActionLinks'));
        add_filter('woocommerce_settings_tabs_array', array($this, 'addSettingsTab'), 50, 1);
        add_action('woocommerce_settings_tabs_merchandise_api', array($this, 'settingsTab'));
        add_action('woocommerce_update_options_merchandise_api', array($this, 'updateSettings'));
        // add api endpoints
        add_action('rest_api_init', array($merchandiseAPI, 'registerRoutes'), 1);
        // add a handler to custom product meta query keys
        add_filter('woocommerce_product_data_store_cpt_get_products_query', array($this, 'handleCustomMetaQueryKeys'), 10, 3);
        add_filter('woocommerce_order_status_completed', array($this, 'handdleWoocomerceEmails'), 0, 1);
    }

    /**
     * Method to implement a singleton pattern
     *
     */
    public static function getInstance($file = null)
    {
        if (!(self::$instance instanceof self)) {
            self::$instance = new self($file);
        }

        return self::$instance;
    }

    /**
     * Attached to activate_{ plugin_basename( __FILES__ ) } by register_activation_hook()
     * @static
     */
    public static function activate()
    {

    }

    /**
     * Attached to deactivate_{ plugin_basename( __FILES__ ) } by register_deactivation_hook()
     * @static
     */
    public static function deactivate()
    {

    }

    /**
     * Attached to uninstall plugin
     * @static
     */
    public static function uninstall()
    {

    }

    public function validateRequirements()
    {
        require_once WC_MERCHANDISE_API_DIR."src/class/CurrencyConverter.php";

        if (version_compare(PHP_VERSION, '7.1.0', '<')) {
            if (is_admin() && !defined('DOING_AJAX')) {
                add_action(
                    'admin_notices',
                    function () {
                        self::adminNotice('WC Merchandise Gateway: plugin fue desarrollado usando PHP 7, algunas funcionalidades podrían fallar en esta versión', 'warning');
                    }
                );
            }
        }

        if (!in_array('woocommerce/woocommerce.php', apply_filters('active_plugins', get_option('active_plugins')), true)) {
            if (is_admin() && !defined('DOING_AJAX')) {
                add_action(
                    'admin_notices',
                    function () {
                        self::adminNotice('Paymegc Merchandise Gateway: Requiere que se encuentre instalado y activo el plugin: Woocommerce', 'error');
                    }
                );
            }
            return false;
        }
        
        if (!in_array('woo-pdf-invoices-packing-slips/woo-pdf-invoices-packing-slips.php', apply_filters('active_plugins', get_option('active_plugins')), true)) {
            if (is_admin() && !defined('DOING_AJAX')) {
                add_action(
                    'admin_notices',
                    function () {
                        self::adminNotice('Paymegc Merchandise Gateway: Requiere que se encuentre instalado y activo el plugin: Paymegc PDF Invoices & Packing Slips', 'error');
                    }
                );
            }
            return false;
        }

        // $local_currency = get_woocommerce_currency();
        $local_currency = apply_filters('woocommerce_currency', get_option('woocommerce_currency'));

        if (!(CurrencyConverter::isConvertible($local_currency))) {
            if (is_admin() && !defined('DOING_AJAX')) {
                add_action(
                    'admin_notices',
                    function () use ($local_currency) {
                        $link = "<a target='_blank' href='https://www.ecb.europa.eu/stats/policy_and_exchange_rates/euro_reference_exchange_rates/html/index.en.html'>See valid currencies</a>";
                        self::adminNotice("WC Merchandise Gateway: invalid currency: {$local_currency}. $link", 'error');
                    }
                );
            }
            return false;
        }

        return true;

    }

    public function setActionLinks($links)
    {
        $plugin_links = array();
        // settings link
        $plugin_links[] = '<a href="' . admin_url('admin.php?page=wc-settings&tab=merchandise_api') . '">Settings</a>';

        return array_merge($links,$plugin_links);
    }

    public function addSettingsTab($tabs)
    {
        $tabs["merchandise_api"] = __('Merchandise API','woocommerce_gateway_merchandise');

        return $tabs;
    }

    public function settingsTab($tabs)
    {
        woocommerce_admin_fields($this->getSettings() );
    }

    protected function getSettings() {
        $settings = include WC_MERCHANDISE_API_DIR . 'src/config/form-fields.php';
        return apply_filters( 'wc_settings_tab_merchandise_api', $settings );
    }

    public function updateSettings() {
        woocommerce_update_options($this->getSettings());
    }

    /**
     * Prevent woocommerce from sending emails when for Merchandise API orders
     *
     * @param class $orderId    id of email's order
     * @return void
     */
    public function handdleWoocomerceEmails($order_id) {
        require_once plugin_dir_path( __FILE__ ) . "WC_Merchandise_Gateway_API.php";

        if(WC_Merchandise_Gateway_API::isOrderMerchandiseAPI($order_id)) {
            remove_filter(current_filter(), 'WC_Emails::send_transactional_email', 10);
        }

        return $order_id;
    }

    /**
     * Handle a custom 'range_prices' query var to get products into a range of prices
     * @param array $query - Args for WP_Query.
     * @param array $query_vars - Query vars from WC_Product_Query.
     * @return array modified $query
     */
    public function handleCustomMetaQueryKeys( $query, $query_vars ) {

        $meta_key = 'range_prices'; // The custom meta_key

        if ( ! empty( $query_vars[$meta_key] ) ) {
            $range = floatval($query_vars[$meta_key]);

            $query['meta_query'][] = array(
                'relation' => 'OR',
                array(
                    'key' => '_price',
                    'value' => $range,
                    'compare' => '<=',
                    'type' => 'DECIMAL(4,2)'
                ),
                array(
                    'key' => '_sales_price',
                    'value' => $range,
                    'compare' => '<=',
                    'type' => 'DECIMAL(4,2)'
                ),
            );
        }

        return $query;
    }

    public static function log($msg)
    {
        if (is_array($msg) || is_object($msg)) {
            $msg = print_r($msg, true);
        }

        $logger = new WC_Logger();
        $logger->add('wc-gateway-merchandise', $msg);
    }

    /**
     * Display an admin notice
     */
    public function adminNotice($notice, $type)
    {
        $class = "notice-info";
        if ($type == "error") {
            $class = "notice-error";
        } elseif ($type == "warning") {
            $class = "notice-warning";
        } elseif ($type == "success") {
            $class = "notice-success";
        }

        echo "<div class='notice $class'>" .
        '<p>' . $notice . '</p>' .
            '</div>';
    }
}
