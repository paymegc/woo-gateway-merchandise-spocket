<?php

class Customer extends WC_Customer {


    /**
     * Look for a customer in the database a return it
     *
     * @param [type] $params
     * @return void
     */
    public static function getCustomer ($params) {
        global $wpdb;

        $customer = array();
	// for debugging
	//$wpdb->show_errors( true );
	$usersTable = $wpdb->prefix."users";
	$usersMetaTable = $wpdb->prefix."usermeta";
        if (isset($params["email"])) {
            $email = sanitize_text_field($params["email"]);
            $data = (array) $wpdb->get_results("SELECT * FROM {$usersTable} INNER JOIN {$usersMetaTable} ON {$usersTable}.id = {$usersMetaTable}.user_id WHERE user_email = '{$email}'");
        } else if (isset($params["id"])) {
            $id = sanitize_text_field($params["id"]);
            $data = (array) $wpdb->get_results("SELECT * FROM {$usersTable} INNER JOIN {$usersMetaTable} ON {$usersTable}.id = {$usersMetaTable}.user_id WHERE {$usersTable}.id = '{$id}'");
        }

        if (!empty($data)) {
                $user = array();
                $user["id"]                 = $data[0]->ID;
                $user["email"]              = $data[0]->user_email;
                $user["user_registered"]    = $data[0]->user_registered;
                $user["user_status"]        = $data[0]->user_status;

                foreach ($data as $key => $value) {
                    $user[$value->meta_key] = $value->meta_value;
                }

                $customer = $user["wp_user_level"] == 0 ? $user : $customer;
            }

        return $customer;
    }
}
