<?php

class Taxes {

    /**
     * Look for the specific tax for a country - state
     *
     * @param string $country   country to match
     * @param string $state     state to match
     * @return void
     */
    public static function getTax ($country, $state) {
        $taxes["US"] = [
            'AL' => 4.00,
            'AK' => 0.00,
            'AZ' => 5.60,
            'AR' => 6.50,
            'CA' => 7.25,
            'CO' => 2.90,
            'CT' => 6.35,
            'DE' => 0.00,
            'DC' => 5.75,
            'FL' => 6.00,
            'GA' => 4.00,
            'HI' => 4.00,
            'ID' => 6.00,
            'IL' => 6.25,
            'IN' => 7.00,
            'IA' => 6.00,
            'KS' => 6.50,
            'KY' => 6.00,
            'LA' => 5.00,
            'ME' => 5.50,
            'MD' => 6.00,
            'MA' => 6.25,
            'MI' => 6.00,
            'MN' => 6.88,
            'MS' => 7.00,
            'MO' => 4.23,
            'MT' => 0.00,
            'NE' => 5.50,
            'NV' => 6.85,
            'NH' => 0.00,
            'NJ' => 6.88,
            'NM' => 5.13,
            'NY' => 4.00,
            'NC' => 4.75,
            'ND' => 5.00,
            'OH' => 5.75,
            'OK' => 4.50,
            'OR' => 0.00,
            'PA' => 6.00,
            'RI' => 7.00,
            'SC' => 6.00,
            'SD' => 4.50,
            'TN' => 7.00,
            'TX' => 6.25,
            'UT' => 4.70,
            'VT' => 6.00,
            'VA' => 4.30,
            'WA' => 6.50,
            'WV' => 6.00,
            'WI' => 5.00,
            'WY' => 4.00,
            'VI' => 6.00,
            'AS' => 6.00,
            'GU' => 6.00,
            'MP' => 6.00,
            'PR' => 6.00
        ];
        
        return $taxes[$country][$state] ? $taxes[$country][$state] : 0.00;
    }
}